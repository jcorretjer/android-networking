package com.retroroots.networking.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.retroroots.networking.R
import com.retroroots.networking.data.remote.FakeApiRepo
import com.retroroots.networking.data.remote.ApiResult
import com.retroroots.networking.data.remote.model.error.FakeApiError
import com.retroroots.networking.data.remote.model.Post
import com.retroroots.networking.data.remote.model.error.ApiError
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var fakeApiRepo: FakeApiRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //todo convertir esto en un view model usando flow
        GlobalScope.launch(Main)
        {
            fakeApiRepo.getSinglePost().run {
                when(this)
                {
                    is ApiResult.Success<*> -> println(this.data as Post)

                    is ApiResult.Error<*> ->
                    {
                        (this.error as FakeApiError).apply {
                            when(type)
                            {
                                ApiError.Type.NO_INTERNET_CONNECTION -> println(this.exception.message)

                                ApiError.Type.SERVER -> println(this.exception.message)

                                else -> println(this.exception.message)
                            }
                        }
                    }
                }
            }
        }

    }
}