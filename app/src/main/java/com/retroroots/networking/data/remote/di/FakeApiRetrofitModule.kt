package com.retroroots.networking.data.remote.di

import com.retroroots.networking.data.remote.api.FakeApiApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object FakeApiRetrofitModule {

    const val TAG = "FAKE_API"

    @Singleton
    @Provides
    @Named(TAG)
    fun providesFakeApiRetrofit() : Retrofit = Retrofit.Builder()
        .baseUrl(FakeApiApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}