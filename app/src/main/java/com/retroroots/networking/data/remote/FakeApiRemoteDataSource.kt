package com.retroroots.networking.data.remote

import com.retroroots.networking.data.remote.api.FakeApiApi
import com.retroroots.networking.data.remote.model.error.ApiError
import com.retroroots.networking.data.remote.model.error.FakeApiError
import java.lang.Exception
import java.net.UnknownHostException

class FakeApiRemoteDataSource(private val fakeApiApi: FakeApiApi) : FakeApiDataSource {
    override suspend fun getSinglePost(): ApiResult {

        try {
            fakeApiApi.getSinglePost().apply {
                return if (isSuccessful && body()!!.id > 0) ApiResult.Success(body()!!) else ApiResult.Error(
                    FakeApiError(ApiError.Type.SERVER, Exception("server error"))
                )
            }
        }

        catch (e: UnknownHostException)
        {
            return ApiResult.Error(FakeApiError(ApiError.Type.NO_INTERNET_CONNECTION, e))
        }

        catch (e: Exception)
        {
            return ApiResult.Error(FakeApiError(ApiError.Type.UNKNOWN, e))
        }
    }
}