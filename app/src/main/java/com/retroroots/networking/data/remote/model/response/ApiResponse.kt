package com.retroroots.networking.data.remote.model.response

interface ApiResponse {
    val data : Any
}