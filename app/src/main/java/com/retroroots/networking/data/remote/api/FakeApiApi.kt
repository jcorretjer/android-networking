package com.retroroots.networking.data.remote.api

import com.retroroots.networking.BuildConfig
import com.retroroots.networking.data.remote.model.Post
import retrofit2.Response
import retrofit2.http.GET

interface FakeApiApi
{
    companion object
    {
        const val BASE_URL = BuildConfig.FAKE_API_BASE_URL
    }

    @GET("posts/1")
    suspend fun getSinglePost(): Response<Post>
}