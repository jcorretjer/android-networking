package com.retroroots.networking.data.remote.model


import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("categoryId")
    val categoryId: Int = 0,
    @SerializedName("content")
    val content: String = "",
    @SerializedName("hits")
    val hits: Int = 0,
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("imageUrl")
    val imageUrl: String = "",
    @SerializedName("likes")
    val likes: Int = 0,
    @SerializedName("title")
    val title: String = "",
    @SerializedName("userId")
    val userId: Int = 0
)