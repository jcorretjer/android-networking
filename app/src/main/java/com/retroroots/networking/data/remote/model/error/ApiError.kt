package com.retroroots.networking.data.remote.model.error

import java.lang.Exception

interface ApiError {
    val type : Type

    val exception : Exception

    enum class Type
    {
        NO_INTERNET_CONNECTION,
        SERVER,
        UNKNOWN
    }
}