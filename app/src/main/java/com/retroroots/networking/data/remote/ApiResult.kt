package com.retroroots.networking.data.remote

sealed class ApiResult {
    data class Success<out S>(val data: S) : ApiResult()

    data class Error<out E>(val error: E) : ApiResult()
}