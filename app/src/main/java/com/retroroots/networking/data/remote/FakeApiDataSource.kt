package com.retroroots.networking.data.remote

interface FakeApiDataSource {
    suspend fun getSinglePost() : Any
}