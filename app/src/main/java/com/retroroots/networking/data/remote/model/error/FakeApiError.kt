package com.retroroots.networking.data.remote.model.error

import java.lang.Exception

data class FakeApiError(override val type: ApiError.Type, override val exception: Exception) : ApiError
