package com.retroroots.networking.data.remote

import com.retroroots.networking.data.remote.api.FakeApiApi
import javax.inject.Inject

class FakeApiRepo @Inject constructor(private val fakeApiApi: FakeApiApi) : FakeApiDataSource {
    override suspend fun getSinglePost() = FakeApiRemoteDataSource(fakeApiApi).getSinglePost()
}