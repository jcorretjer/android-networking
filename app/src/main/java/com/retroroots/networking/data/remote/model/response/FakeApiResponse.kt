package com.retroroots.networking.data.remote.model.response

data class FakeApiResponse(override val data: Any) : ApiResponse
