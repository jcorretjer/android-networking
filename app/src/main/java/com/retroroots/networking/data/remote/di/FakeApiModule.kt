package com.retroroots.networking.data.remote.di

import com.retroroots.networking.data.remote.api.FakeApiApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object FakeApiModule {
    @Singleton
    @Provides
    fun providesFakeApiApi(@Named(FakeApiRetrofitModule.TAG) retrofit: Retrofit) : FakeApiApi =
        retrofit.create(FakeApiApi::class.java)
}